import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

var config = {
    apiKey: "AIzaSyBwsH3HLbqMyuZUaINl6dFoHG43bvIQmiE",
    authDomain: "movie-app-4efbd.firebaseapp.com",
    databaseURL: "https://movie-app-4efbd.firebaseio.com",
    projectId: "movie-app-4efbd",
    storageBucket: "movie-app-4efbd.appspot.com",
    messagingSenderId: "413914144831"
  };
  firebase.initializeApp(config);

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}
