import React from 'react';
import { Card } from 'antd';
import { connect } from 'react-redux';
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
    return {
      onItemMovieClick: item =>
        dispatch({
          type: 'click_item',
          payload: item
        })
    };
  };
  



function ItemGif(props){
    const item = props.item;

    return(
        <Card
      onClick={() => {
        props.onItemMovieClick(item);
      }}
    // style={{ height: 600 }}
      hoverable
      cover={<img alt="example" src={item.images.fixed_width.url} // change
      style={{ height: 400 }}
      />}
    >
      <Meta
        title={item.title}
      />
    </Card>
    )
}

export default connect(
    null,
    mapDispatchToProps
)(ItemGif);