import React, { Component } from 'react';
import { List } from 'antd';
import ItemFavorite from './item';

class ListFavorite extends Component {
    state = {
        items: []
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem('list-fav');
        if (jsonStr) {
            const items = JSON.parse(jsonStr);
            console.log('fav', items);
            this.setState({ items });
        }
    }
    render() {
        return (
            <List
            pagination={{
      
                pageSize: 40,
              }}
                //   grid={{ gutter: 16, column: 4 }}
                dataSource={this.state.items}
                grid={{ gutter: 16, column: 4 }}
                renderItem={item => (
                    <List.Item>
                        <ItemFavorite item={item} onItemMovieClick={this.props.onItemMovieClick} />
                    </List.Item>
                )}
            />
        )
    }
}
export default ListFavorite;