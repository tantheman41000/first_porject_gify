import React ,{Component} from 'react';
import { Card } from 'antd';
import { connect } from 'react-redux';
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
    return {
      onItemMovieClick: item =>
        dispatch({
          type: 'click_item',
          payload: item
        })
    };
  };


  class ItemFavorite extends Component{
      render(){
        const item = this.props.item;
          return(
            <Card
            style={{
              alignItems: 'center',
              maxHeight: '50%'
          }}
            onClick={() => {
              this.props.onItemMovieClick(item);
            }}
             
            hoverable
            cover={<img alt="example" src={item.images.fixed_width.url} 
            style={{ height: 300 }}
            />}
          >
            <Meta
              title={item.title}
            />
          </Card>
          )
      }
  }
  export default connect(
    null,
    mapDispatchToProps
)(ItemFavorite);